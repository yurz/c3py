### Very simple wrapper for plotting C3.JS charts inside of Jupyter/IPython and rendering html files.
[Sample Jupyter/IPython Notebook](http://nbviewer.jupyter.org/urls/bitbucket.org/yurz/c3py/raw/master/sample/c3py-sample.ipynb)

### PIP Install:
`pip install --upgrade git+https://bitbucket.org/yurz/c3py`