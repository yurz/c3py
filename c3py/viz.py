from . import export
from warnings import warn


@export
class C3Chart():

    head = """
            <head>
              <script src="http://ajax.googleapis.com/ajax/libs/jquery/2.1.3/jquery.min.js"></script>
              <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.4/js/bootstrap.min.js"></script>
              <script src="http://d3js.org/d3.v3.min.js" charset="utf-8"></script>
              <script src="https://cdnjs.cloudflare.com/ajax/libs/c3/0.4.18/c3.min.js"></script>
              <link href="https://cdnjs.cloudflare.com/ajax/libs/c3/0.4.18/c3.min.css" rel="stylesheet"/>
              <style>
                body {
                    font-family: Verdana, Arial, serif;
                }
                .c3cont {
                    float:left;
                    width:100%;
                    margin-top: 20px;
                    margin-bottom: 32px;
                    margin-left: 20px;
                }
                .c3chart {
                    float:left;
                    width:100%;
                    margin-top: 20px;
                }
                .c3-line { stroke-width: 3px; }
                .c3-legend-item-hidden {
                    opacity: .5 !important;
                }
                .chrt-btn {
                    color: white;
                    background-color: #4791bc;
                    border-color: #4791bc;
                    border-radius: 2px;
                    text-align: center;
                    text-decoration: none;
                    display: inline;
                    float:left;
                }
                .chrt-title {
                    display: inline;
                    margin-left: 16px;
                    font-size: 120%;
                }
                </style>
              </head>
            """


    ipylibs = """<script>
            require.config({
            baseUrl: "",
            paths: {
                    d3: "//cdnjs.cloudflare.com/ajax/libs/d3/3.4.8/d3.min",
                    c3: "//cdnjs.cloudflare.com/ajax/libs/c3/0.4.10/c3.min"
                  }
            });
            $("head").append('<link href="https://cdnjs.cloudflare.com/ajax/libs/c3/0.4.10/c3.min.css" rel="stylesheet" />');
            $("head").append('<style>\
                .c3chart {\
                    float:left;\
                    width:100%;\
                    margin-top: 20px;\
                }\
                .c3-line { stroke-width: 3px; }\
                .c3-legend-item-hidden {\
                    opacity: .5 !important;\
                }\
                .chrt-btn {\
                    color: white;\
                    background-color: #4791bc;\
                    border-color: #4791bc;\
                    border-radius: 2px;\
                    text-align: center;\
                    text-decoration: none;\
                    display: inline;\
                    float:left;\
                }\
                .chrt-title {\
                    display: inline;\
                    margin-left: 16px;\
                    font-size: 120%;\
                }</style>');
            require(["d3", "c3"], function(d3, c3) {
            window.d3 = d3;
            window.c3 = c3;
            });
            </script>
            """


    def ipy_load(silent=False):
        from IPython.display import Javascript, HTML, display_html
        display_html(HTML(C3Chart.ipylibs))
        if not silent:
	        print("c3 loaded")


    def df2c3(df, decimals=1):
        import numpy as np
        data = {}
        categories = list(df.index.values)
        columns = []
        groups = []
        for col in df.columns:
            this_col = [col]
            groups.append(col)
            vals = list(df[col].values)
            vals = [("{:." + str(decimals) + "f}").format(v) for v in vals]
            this_col.extend(vals)
            columns.append(this_col)
        data["columns"] = columns
        data["categories"] = categories
        data["groups"] = [groups]
        return data


    def id_gen(size=3):
        import string
        import random
        chars = string.ascii_letters
        return ''.join(random.choice(chars).lower() for x in range(size))


    def __init__(self, data=None, libload=None, templ=None, id=None, kind=None, width=None,
        height=None, title=None, x_tick_culling=None, stack=None, decimals=None, css=None, rotate=None):

        if data is not None:
            self.data = data

        if templ:
            self.templ = templ
        else:
            self.templ = """
            <div class="c3cont">
                <style>&&css</style>
                <div style="width:100%">
                    <input type="button" class="chrt-btn" onClick="&&chrt_id.show()" value="Show All" />
                    &nbsp;&nbsp;
                    <input type="button" class="chrt-btn" onClick="&&chrt_id.hide()" value="Hide All" />
                    <span class="chrt-title">&&title</span>
                </div>

                <div id='&&chrt_id' class="c3chart"></div>
                <br/><br/>
                <script>
                var data = &&data;

                var &&chrt_id = c3.generate({
                    bindto: "#&&chrt_id",
                    size: {height: &&height, width: &&width},
                     padding: {
                                top: 10,
                                right: 40,
                                bottom: 10,
                                left: 40,
                            },
                    data: {
                        columns: data.columns,
                        type: "&&type",
                        groups: data.groups,
                        order: function (a, b) {
                            return a.id < b.id;
                        }
                    },
                    axis: {
                            rotated: &&rotate,
                            x: {
                                type:"category",
                                categories: data.categories,
                                tick: {
                                    fit: true,
                                    culling: { max: &&x_tick_culling },
                                    multiline: false
                                    }
                                },
                            y: {
                                  tick: {
                                      format: d3.format("s")
                                  }
                              }
                          },
                    grid: {
                            x: {show: true},
                            y: {show: true}
                        },

                    tooltip: {
                        format: {
                            value: function (value, ratio, id) {
                                var format = d3.format(",");
                                if (value != 0) {
                                  return format(value);
                                }
                             }
                           }
                        }
                });
                </script>
                </div>
            """


        if id:
            id = ''.join(ch for ch in id if ch.isalnum() or ch == "-")
            self.id = id
        else:
            self.id = "chrt_{}".format(C3Chart.id_gen())

        if kind:
            self.kind = kind
        else:
            self.kind = "line"

        if title:
            self.title = title
        else:
            self.title = ""

        if width:
            self.width = width
        else:
            self.width = 900

        if height:
            self.height = height
        else:
            self.height = 400

        if x_tick_culling:
            self.x_tick_culling = x_tick_culling
        else:
            self.x_tick_culling = 12

        if stack is None:
            self.stack = False
        else:
            self.stack = stack

        if decimals is None:
            self.decimals = 1
        else:
            self.decimals = decimals

        if css is None:
            self.css = ""
        else:
            self.css = css

        if rotate is True:
            self.rotate = "true"
        else:
            self.rotate = "false"


    def to_html(self):
        from pandas import DataFrame
        if self.data is None:
            warn("data has to be provided")
        data = self.data
        if isinstance(data, DataFrame):
            data = C3Chart.df2c3(data, decimals=self.decimals)
            if "area" in self.kind:
                self.stack = True
            if self.stack == False or self.stack is None:
                data["groups"] = []
            if self.kind.lower() in ["line", "spline"]:
                data["groups"] = []
        return self.templ.replace("&&data", \
               str(data)).replace("&&chrt_id", \
               self.id).replace("&&type", \
               self.kind).replace("&&width", \
               str(self.width)).replace("&&height", \
               str(self.height)).replace("&&x_tick_culling", \
               str(self.x_tick_culling)).replace("&&title", \
               str(self.title)).replace("&&css", \
               str(self.css)).replace("&&rotate", \
               str(self.rotate))


    def copy(self):
        import copy
        chrt = copy.deepcopy(self)
        chrt.id = chrt.id + "_copy"
        return chrt


    def __call__(self):
        from IPython.display import HTML
        return HTML(self.to_html())
