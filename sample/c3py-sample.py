
# coding: utf-8

# ### This is sample of using c3py library (https://yurz@bitbucket.org/yurz/c3py.git)

# #### Only Line, Area and Bar (Unstacked and Stacked) charts are supported at the momen. c3py can be used for displaying charts inside Jupyter Notebooks or rendering self-contained HTML files.

# In[1]:

from c3py import C3Chart as c3


# In[2]:

import sys
import IPython


# In[3]:

print(sys.version)
print(IPython.__version__)


# #### For displaying inside Jupyter - Load required Javascript libraries:

# In[4]:

c3.ipy_load()


# #### Download some GDP data from World Bank and prepare Pandas DataFrame object:

# In[12]:

from pandas_datareader import data, wb


# In[17]:

df = wb.download(indicator='NY.GDP.PCAP.PP.KD', country="all", start=2010, end=2015).unstack()


# #### This is how raw df looks:

# In[18]:

df.head()


# #### Cleanup and create df_selected with top 5 and bottom 5 countries based on GDP Per Capita Growth between 2010 and 2015:

# In[19]:

df.columns = df.columns.levels[1]
df = df.dropna(how="any")
df = df.astype(int)
df["5yr_change"] = (df["2014"]-df["2010"])/df["2010"]
df = df.sort_values("5yr_change")
df_selected = df.head().append(df.tail())
df_selected


# In[20]:

ch1 = c3(data=df_selected[['2010', '2011', '2012', '2013', '2014', '2015']].T, 
         width=1020, height=380, kind="line", title="GDP Per Capita 2010-2015 Growth - Top 5 and Bottom 5 Countries", id="ch1")
ch1()


# #### Now lat's create a copy of the above chart presented as an area chart

# In[21]:

ch2 = ch1.copy()


# In[22]:

ch2.id = "ch2"
ch2.kind = "area"
ch2()


# #### Render a HTML file with both files:

# In[23]:

with open("sample.html", "w") as f:
    f.write("<html><body>" + c3.head + ch1.to_html() + "<br/><br/>" + ch2.to_html() + "</html></body>")


# [Yuri Zhylyuk](http://yznotes.com) 2016-10-23
