import os

from setuptools import setup, find_packages

def read(*paths):
    """Build a file path from *paths* and return the contents."""
    with open(os.path.join(*paths), 'r') as f:
        return f.read()

setup(
    name='c3py',
    version='0.0.9',
    py_modules=['data visualization', 'charting'],
    url='https://bitbucket.org/yurz/c3py',
    description='Wrapper for plotting with C3.JS library - inside of Jupyter/IPython or by rendering html files.',
    long_description=(read('README.md')),
    license='MIT',
    author='Yuri Zhylyuk',
    author_email='yuri@zhylyuk.com',
    include_package_data=True,
    classifiers=[
        'Natural Language :: English',
        'Operating System :: OS Independent',
        'Programming Language :: Python :: 3',
        'Topic :: Software Development :: Libraries :: Python Modules :: Data Visialisation',
    ],
    packages=find_packages(exclude=['tests*', 'misc*']),
    install_requires=['numpy', 'pandas']
)
